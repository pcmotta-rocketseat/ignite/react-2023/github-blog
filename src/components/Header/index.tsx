import { HeaderContainer } from './styles'

import backgroundHeader from '../../assets/background-header.svg'

export function Header() {
  return (
    <HeaderContainer>
      <img src={backgroundHeader} alt="" />
    </HeaderContainer>
  )
}
